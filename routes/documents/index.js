const express = require('express');
const Upload = require('./upload');
const fileUpload = require('express-fileupload');

module.exports = class TestRoute {
  constructor(middlewares, models, modules) {
    this.upload = new Upload(modules);
    this._tokenAuth = middlewares.tokenAuth;
  }

  initRoutes(app) {
    const api = express.Router();
    api.post('/upload',
      this._tokenAuth.handler,
      fileUpload({
        useTempFiles : true,
        tempFileDir : '/tmp/'
      }),
      this.upload.handler
    );
    app.use('/docs', api);
  }
};
