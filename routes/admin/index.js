const express = require('express');
const CreateRootAdmin = require('./create-root-admin');
const AdminLogin = require('./admin-login');
const VerifyAdminToken = require('./verify-admin-token');

module.exports = class TestRoute {
  constructor(middlewares, models, modules, config) {
    this._tokenAuth = middlewares.tokenAuth;
    this._createRootAdmin = new CreateRootAdmin(modules, config);
    this._adminLogin = new AdminLogin(modules);
    this._verifyAdminToken = new VerifyAdminToken(modules);
  }

  initRoutes(app) {
    const api = express.Router();
    api.post('/:appId/create-root', this._tokenAuth.fmklHandler, this._createRootAdmin.handler);
    api.post('/:appId/authenticate', this._adminLogin.handler);
    api.get('/:appId/verify-token', this._tokenAuth.handler, this._verifyAdminToken.handler);
    app.use('/admin', api);
  }
};
