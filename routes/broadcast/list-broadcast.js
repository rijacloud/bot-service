const httpStatus = require('http-status');

module.exports = class CreateBroadcast {
  constructor(modules) {
    this._broadcastManager = modules.broadCastManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      let appId = req.params.appId
      await this._appManager.checkAppId(appId);  
      return (await this._getList(res, appId));
    } catch (e) {
      return next(e);
    }
  }

  async _getList(res, appId) {
    const entry = await this._broadcastManager.getList(appId);
    return res.status(httpStatus.OK).json({ broadcast: entry });
  }

}