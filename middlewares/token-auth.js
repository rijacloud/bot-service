const httpStatus = require('http-status');

module.exports = class TokenAuth {
  constructor(modules) {
    this.handler = this.handler.bind(this);
    this.fmklHandler = this.fmklHandler.bind(this);
    this._sessionManager = modules.sessionManager;
  }

  async handler(req, res, next) {
    try {
      const authorization = this._sessionManager.retrieveHeaderToken(req);
      if (!authorization) return this._missingAuthorization(res);
      const { adminId, appId } = await this._sessionManager.verifyToken(authorization);
      req.adminId = adminId;
      req.appId = appId;
      return next();
    } catch (e) {
      if (e === 'TokenExpiredError') return this._tokenExpired(res);
      if (e === 'JsonWebTokenError') return this._invalidToken(res);
      return next(e);
    }
  }

  async fmklHandler(req, res, next) {
    try {
      const authorization = this._sessionManager.retrieveHeaderToken(req);
      if (!authorization) return this._missingAuthorization(res);
      const payload = await this._sessionManager.verifyFmklToken(authorization);
      return next();
    } catch (e) {
      if (e === 'TokenExpiredError') return this._tokenExpired(res);
      if (e === 'JsonWebTokenError') return this._invalidToken(res);
      return next(e);
    }
  }

  _missingAuthorization(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Missing authorization'
    });
  }

  _invalidToken(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Invalide token'
    });
  }

  _tokenExpired(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Expired token'
    });
  }
};
