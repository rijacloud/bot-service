const express = require('express');
const Test = require('./test');

module.exports = class TestRoute {
  constructor(middlewares, models, modules, config) {
    this.test = new Test(models, modules, config);
    this._tokenAuth = middlewares.tokenAuth;
  }

  initRoutes(app) {
    const api = express.Router();
    api.get('/', this.test.handler);
    app.use('/test', api);
  }
};
