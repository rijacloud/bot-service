const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { TYPE, CHANNEL } = require('../constants/app');
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  name: { type: String, uniq: true, required: true },
  app: {
    type: ObjectId,
    required: true
  },
  data : {
    type: ObjectId, required: true //ObjectId
  },
  executionDate: {
      type: Object,
      required: false
  }
}, {
  timestamps: true
});
const App = mongoose.model('broadcast', schema);

module.exports = App;
