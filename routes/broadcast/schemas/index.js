const Joi = require('joi')

const broadcastSchema = Joi.compile({
    data : Joi.string().required(),
    executionDate: Joi.optional(),
    name: Joi.string().required()  
})

module.exports = {
    broadcastSchema
}