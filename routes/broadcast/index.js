const express = require('express');
const createBroadcast = require('./create-broadcast')
const listBroadcast = require('./list-broadcast')
const getBroadcast = require('./get-broadacast')
const deleteBroadcast = require('./delete-broadcast')
const callBroadcast = require('./call-broadcast')

module.exports = class BroadcastRoute {
  constructor(middlewares, models, modules, config) {
    this._interserviceAuth = middlewares.interserviceAuth;
    this._tokenAuth = middlewares.tokenAuth;
    this._createBroadcast = new createBroadcast(modules)
    this._listBroadcast = new listBroadcast(modules)
    this._getBroadcast = new getBroadcast(modules)
    this._deleteBroadcast = new deleteBroadcast(modules)
    this.callBroadcast = new callBroadcast(modules)
  }

  initRoutes(app) {
    const api = express.Router();
    api.post('/:appId/create',this._tokenAuth.handler, this._createBroadcast.handler)
    api.get('/:appId/list',this._tokenAuth.handler, this._listBroadcast.handler)
    //api.get('/:appId/:id/get',this._tokenAuth.handler, this._getBroadcast.handler)
    api.post('/generate', this._tokenAuth.handler, this._createBroadcast.handler)
    api.delete('/:appId/:id/delete', this._tokenAuth.handler, this._deleteBroadcast.handler)
    api.post('/call', this.callBroadcast.handler)
    app.use('/broadcast', api);
  }
};
