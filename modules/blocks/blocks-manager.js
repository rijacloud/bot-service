const { TYPE } = require('../../constants/blocks');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
const request = require('request-promise');

module.exports = class BlocksManager {
  constructor(models) {
    this._textBlockModel = models.textBlockModel;
    this._carousselBlockModel = models.carousselBlockModel;
    this._quickReplyBlockModel = models.quickReplyBlockModel;
    this._buttonBLockModel = models.buttonBLockModel;
    this._containerBlockModel = models.containerBlockModel;
    this._menuBlockModel = models.menuBlockModel;
    this._mediaBlockModel = models.mediaBlockModel;
    this._modelMap = {
      [TYPE.TEXT]: '_textBlockModel',
      [TYPE.CAROUSSEL]: '_carousselBlockModel',
      [TYPE.QUICK_REPLY]: '_quickReplyBlockModel',
      [TYPE.BUTTON]: '_buttonBLockModel',
      [TYPE.CONTAINER]: '_containerBlockModel',
      [TYPE.MENU]: '_menuBlockModel',
      [TYPE.MEDIA]: '_mediaBlockModel'
    };
  }

  async checkName(appId, name, type) {
    const count = await this._getModel(type).countDocuments({ app: appId, name});
    if (count) {
      const error = Error(`Name ${name} already taken under the app ${appId}`);
      error.statusCode = httpStatus.BAD_REQUEST;
      throw error;
    }
    return true;

  }

  async isBLockExists(appId, name, type) {
    const count = await this._getModel(type).countDocuments({ app: appId, name});
    if (!count) {
      const error = Error(`block with name ${name} does not exosts under the app ${appId}`);
      error.statusCode = httpStatus.BAD_REQUEST;
      throw error;
    }
    return true;
  }

  async checkBlockId(blockId, type) {
    const count = await this._getModel(type).countDocuments({ _id: blockId });
    if (!count) {
      const error = Error(`block ${blockId} not found`);
      error.statusCode = httpStatus.NOT_FOUND;
      throw error;
    }
    return true;
  }

  countById(blockId, type) {
    return this._getModel(type).countDocuments({ _id: blockId });
  }

  getById(blockId, type) {
    return this._getModel(type).findById(blockId).lean();
  }

  update(type, blockId, data) {
    return this._getModel(type).findOneAndUpdate(
      { _id: blockId },
      {
        $set: { data }
      },
      { new: true, useFindAndModify: true }
    ).lean();
  }

  async getByAppId(appId) {
    const containers = await this._containerBlockModel.find({ app: appId }).lean();
    for (let i = 0; i < containers.length; i++) {
      for (let j = 0; j < containers[i].data.length; j++) {
        containers[i].data[j] = await this._getModel(containers[i].data[j].type)
          .findById(containers[i].data[j].id).lean();
      }

    }
    return containers;
  }

  deleteBlockById(blockId, type) {
    return this._getModel(type).remove({ _id: blockId });
  }

  async populateContainer(block) {
    for (let i = 0; i < block.data.length; i++) {
      block.data[i] = await this._getModel(block.data[i].type)
        .findById(block.data[i].id).lean();
    }
    return block;
  }

  async populateAndClean(block) {
    for (let i = 0; i < block.data.length; i++) {
      block.data[i] = await this._getModel(block.data[i].type)
        .getAndClean(block.data[i].id);
    }
    return block;
  }

  _getModel(type) {
    return this[`${this._modelMap[type]}`];
  }

  getByName(appId, name, type) {
    return this._getModel(type).getByName({ app: appId, name }).lean();
  }

  create(appId, type, name, data) {
    return this._getModel(type).create({
      app: appId,
      type,
      data,
      name
    })
  }
}