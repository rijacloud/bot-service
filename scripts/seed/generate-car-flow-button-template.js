const { APP } = require('../../config');
const request = require('request-promise');

const SessionManager = require('../../modules/session-manager');
const templates = require('./template/car-flow-buttons-template');

const sessionManager = new SessionManager({
  session: APP.session,
  fmklCreatorSession: APP.fmklCreatorSession
});

const payload = {
  random: true
};

const auth = sessionManager.signFmkl(payload);

const post = (url, data) => {
  return request({
    url,
    headers: {
      authorization : auth
    },
    timeout: 120000,
    method: 'POST',
    json: data
  })
}

const run = async () => {
  try {
    const keys = Object.keys(templates);
    const length = keys.length;
    const url = `http://127.0.0.1:4500/blocks/${APP.karenjyApp.karenjyAppId}/create?type=2&name=`
    for (let i = 0; i < length; i ++) {
      let key = keys[i];
      let data = templates[key];
      await post(
        url + key,
        data
      );
      console.log(`Processed ${ i + 1 } / ${length}`);
    }
  } catch (e) {
    console.log(e);
  }
}

run();