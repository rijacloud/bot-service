const httpStatus = require('http-status');
const { TYPE } = require('../../constants/blocks');

module.exports = class DeleteContainer {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const { blockId } = await this._getParameter(req.params);
      const container = await this._blocksManager.getById(blockId, TYPE.CONTAINER);
      await this.removeElements(container.data);
      await this._blocksManager.deleteBlockById(blockId, TYPE.CONTAINER);
      return res.status(httpStatus.OK).json({ blockId });
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(params) {
    let blockId = params.blockId;
    if (!blockId) this._badRequest('blockId parameter is required');
    await this._blocksManager.checkBlockId(blockId, TYPE.CONTAINER);
    return { blockId };
  }

  async removeElements(elements) {
    for (let i = 0; i < elements.length; i++) {
      let element = elements[i];
      await this._blocksManager.deleteBlockById(element.id, element.type);
    }
  }
}