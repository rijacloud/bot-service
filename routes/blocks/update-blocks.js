const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE } = require('../../constants/blocks');
const {
  carousselBlockSchema,
  quickReplyBlockSchema,
  buttonBLockSchema,
  textBlockSchema,
  containerBLockSchema,
  menuBLockSchema,
  mediaBLockSchema
} = require('./schemas');
const _ = require('lodash');

module.exports = class UpdateBLocks {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this._botClient = modules.botClient;
    this._appManager = modules.appManager;

    this.handler = this.handler.bind(this);

    this._schemaMap = {
      [TYPE.TEXT]: textBlockSchema,
      [TYPE.CAROUSSEL]: carousselBlockSchema,
      [TYPE.QUICK_REPLY]: quickReplyBlockSchema,
      [TYPE.BUTTON]: buttonBLockSchema,
      [TYPE.MENU]: menuBLockSchema,
      [TYPE.MEDIA]: mediaBLockSchema
    };
  }

  async handler(req, res, next) {
    try {
      const { blockId } = await this._getParameter(req.params);
      const data = this._validate(req.body, containerBLockSchema);
      this._validateBLocks(req.body);
      const elements = await this._saveElements(req.appId, data.data);
      const updated = await this._blocksManager.update(TYPE.CONTAINER, blockId, elements);
      const block = await this._blocksManager.populateContainer(updated);
      return res.status(httpStatus.OK).json({ block });
    } catch (e) {
      return next(e);
    }
  }

  _validateBLocks(body) {
    const types = Object.values(TYPE);
    const valid = body.data.every(block => {
      return this._validate(block.entry, this._schemaMap[block.type]) &&
        types.includes(block.type);
    });
    if (valid) return true;
    this._badRequest('incorrect type');
  }

  async _saveElements(app, data) {
    const result = [];
    for (let i = 0; i < data.length; i++) {
      let current = data[i];
      if (await this._blocksManager.countById(current._id, current.type)) {
        await this._blocksManager.update(current.type, current._id, current.entry);
        result.push({ type: current.type, id: current._id });
        if (current.type === TYPE.MENU) {
          const appInstance = await this._appManager.getAppById(app);
          const body = current.entry.map(button => {
            return _.omit(button, ['_id']);
          });
          await this._botClient.updateMenu(appInstance.botHost, body);
        }
      } else {
        let entry = await this._blocksManager.create(
          app,
          current.type,
          current.name,
          current.entry
        );
        result.push({ type: current.type, id: entry._doc._id });
      }
    }
    return result;
  }

  _validate(data, schema) {
    const result = Joi.validate(data, schema);
    if (result.error) {
      console.log(result.error)
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }

  async _getParameter(params) {
    let blockId = params.blockId;
    if (!blockId) this._badRequest('blockId parameter is required');
    await this._blocksManager.checkBlockId(blockId, TYPE.CONTAINER);
    return { blockId };
  }

  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }
};
