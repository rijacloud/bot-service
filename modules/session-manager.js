const jwt = require('jsonwebtoken');

module.exports = class SessionManager {
  constructor(config) {
    this._sessionConfig = config.session;
    this._fmklCreatorSession = config.fmklCreatorSession;
  }

  signIn(data) {
    return this._sign(data, this._sessionConfig);
  }

  signFmkl(data) {
    return this._sign(data, this._fmklCreatorSession);
  }

  _sign(payload, session) {
    return jwt.sign(payload, session.privateKey, { expiresIn: session.expiration });
  }

  verifyToken(token) {
    return this._checkToken(token, this._sessionConfig);
  }

  verifyFmklToken(token) {
    return this._checkToken(token, this._fmklCreatorSession);
  }

  retrieveHeaderToken(req) {
    return req.headers.authorization;
  }

  _checkToken(token, session) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, session.privateKey, (err, decoded) => {
        if (err) return reject(err.name);
        return resolve(decoded);
      });
    });
  }
};
