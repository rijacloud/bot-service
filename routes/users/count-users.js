const httpStatus = require('http-status');

module.exports = class CountUser {
  constructor(modules) {
    this._usersManager = modules.usersManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const count = await this._usersManager.countUsersByAppId(req.appId);
      return res.status(httpStatus.OK).json({ count });
    } catch (e) {
      return next(e);
    }
  }
};