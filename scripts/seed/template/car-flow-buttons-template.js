module.exports = {
  askAirConditioner: {
    "text": "Voulez-vous la climatisation?",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "Oui",
        "payload": "customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=1&step=3&"
      },
      {
        "content_type": "text",
        "title": "Non",
        "payload": "customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=0&step=3&"
      }
    ]
  },
  askPrestige: {
    "text": "👉",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "Oui",
        "payload": "customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=${param.ac}&prestige=1&step=done&"
      },
      {
        "content_type": "text",
        "title": "Non",
        "payload": "customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=${param.ac}&prestige=0&step=done&"
      }
    ]
  },
  giveCarPrice: {
    "text": "Pour recevoir une offre personnalisée,  choisissez votre modalité paiement préférentiel. 💳 💰",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "Comptant",
        "payload": "payement_mode_cash_0_0"
      },
      {
        "content_type": "text",
        "title": "Facilité de paiement",
        "payload": "payement_mode_credit-cash_0_0"
      },
      {
        "content_type": "text",
        "title": "Location LD sur 36 mois",
        "payload": "payement_mode_location_0_0"
      }
    ]
  },
  firstTime: {
    "text": "👉",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "Configurer votre voiture",
        "payload": "discover_cars_0_0_0"
      },
      {
        "content_type": "text",
        "title": "Notre communaute",
        "payload": "community_0_0_0_0"
      },
      {
        "content_type": "text",
        "title": "FAQ",
        "payload": "faq_0_0_0_0"
      }
    ]
  }
};
