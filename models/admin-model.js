const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  app: { type: ObjectId, require: true, ref: 'app' },
  email: { type: String, required: true },
  hash: { type: String, required: true },
  access: [{ type: Number, required: true }]
}, {
  timestamps: true
});
const Admin = mongoose.model('admin', schema);

module.exports = Admin;
