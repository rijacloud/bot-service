const httpStatus = require('http-status');
const { objectIdPattern } = require('../../constants/regexp');

module.exports = class AppManager {
  constructor(models) {
    this._appModel = models.appModel;
  }

  async checkName(name) {
    const count = await this._appModel.count({ name });
    if (count) {
      const error = new Error(`App name ${name} already taken`);
      error.statusCode = httpStatus.BAD_REQUEST;
      throw error;
    }
    return true;
  }

  create(name, type, options = {}) {
    const payload = { name, type };
    if (options.channel) payload.channel = options.channel;
    if (options.botHost) payload.botHost = options.botHost;
    return this._appModel.create(payload);
  }

  async getAppById(appId) {
    await this.checkAppId(appId)
    return this._appModel.findOne({ _id: appId }).lean();
  }

  list() {
    return this._appModel.find({}).lean();
  }

  async checkAppId(id) {
    if (!id) this._badRequest('id is required');
    if (!objectIdPattern.test(id)) this._badRequest('not a valid object Id');
    const count = await this._appModel.countDocuments({ _id: id });
    if (!count) this._badRequest(`No app matching id ${id}`);
    return id;
  }

  _badRequest(message) {
    const error = new Error(message);
    error.status = httpStatus.BAD_REQUEST;
    throw error;
  }
}