const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE } = require('../../constants/blocks');
const {
  carousselBlockSchema,
  quickReplyBlockSchema,
  buttonBLockSchema,
  textBlockSchema,
  menuBLockSchema,
  containerBLockSchema,
  mediaBLockSchema
} = require('./schemas');
const _ = require('lodash');

module.exports = class CreateBlock {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this._appManager = modules.appManager;
    this._botClient = modules.botClient;

    this.handler = this.handler.bind(this);

    this._schemaMap = {
      [TYPE.TEXT]: textBlockSchema,
      [TYPE.CAROUSSEL]: carousselBlockSchema,
      [TYPE.QUICK_REPLY]: quickReplyBlockSchema,
      [TYPE.BUTTON]: buttonBLockSchema,
      [TYPE.MENU]: menuBLockSchema,
      [TYPE.MEDIA]: mediaBLockSchema
    };
  }

  async handler(req, res, next) {
    try {
      const { name, appId } = await this._getParameter(req);
      return (await this._addBlock(
        { appId, name },
        req.body, res
      ));
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(req) {
    const { query, params } = req;
    const name = query.name;
    const appId = req.appId || params.appId;
    if (!name) this._badRequest('name query parameter is required');
    await this._blocksManager.checkName(appId, name, TYPE.CONTAINER);
    await this._appManager.checkAppId(appId);
    return { name, appId };
  }

  async _addBlock({ appId, name }, body, res) {
    body = this._validate(body, containerBLockSchema);
    this._validateBLocks(body);
    const entries = await this._createEntries(body.data, appId);
    const container = await this._blocksManager.create(
      appId,
      TYPE.CONTAINER,
      name,
      entries
    );

    const block = await this._blocksManager.populateContainer(container._doc);

    return res.status(httpStatus.CREATED).json({ block });
  }

  async _createEntries(data, appId) {
    const result = [];
    for (let i = 0; i < data.length; i++) {
      let current = data[i];
      await this._blocksManager.checkName(appId, current.name, current.type);
      let entry = await this._blocksManager.create(
        appId,
        current.type,
        current.name,
        current.entry
      );
      result.push({ type: current.type, id: entry._doc._id });
      if (current.type === TYPE.MENU) {
        const appInstance = await this._appManager.getAppById(appId);
        const body = current.entry.map(button => {
          return _.omit(button, ['_id']);
        });
        await this._botClient.updateMenu(appInstance.botHost, body);
      }
    }
    return result;
  }

  _validateBLocks(body) {
    const types = Object.values(TYPE);
    const valid = body.data.every(block => {
      return this._validate(block.entry, this._schemaMap[block.type]) &&
        types.includes(block.type);
    });
    if (valid) return true;
    this._badRequest('incorrect type');
  }

  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }

  _validate(body, schema) {
    const result = Joi.validate(body, schema);
    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }
}