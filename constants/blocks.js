module.exports = {
  TYPE: {
    TEXT: 0,
    CAROUSSEL: 1,
    QUICK_REPLY: 2,
    BUTTON: 3,
    MEDIA: 4,
    CONTAINER: 10,
    MENU: 11,
  }
};
