const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const button = new Schema({
  type: { type: String, required: true },
  title: { type: String, required: true },
  payload: { type: String, required: true }
});

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: {
    type: [button],
    required: true
  },
  name: { type: String, required: true }
}, {
  timestamps: true
});

const projection = { 'data._id': 0 };

schema.statics.getByName = function({ app, name}) {
  return this.findOne({ app, name }, projection);
};

schema.statics.getAndClean = function(id) {
  return this.findById(id, projection).lean();
};

const MenuBLock = mongoose.model('menuBLock', schema);

module.exports = MenuBLock;