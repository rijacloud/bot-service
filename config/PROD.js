module.exports = {
  APP: {
    port: process.env.FMKL_SERVICE_PORT,
    mongoConnectionString: process.env.FMKL_SERVICE_MONGO_STRING,
    saltRounds: 10,
    session: {
        privateKey: process.env.FMKL_SERVICE_SESSION_KEY,
        expiration: '5d'
    },
    fmklCreatorSession: {
      privateKey: process.env.FMKL_SERVICE_CREATOR_SESSION_KEY,
      expiration: '1d'
    },
    interserviceKey: process.env.FMKL_INTERSERVICE_KEY,
    interserviceAuthExpiration: '5s',
    cloudinaryConfig: {
      url: 'cloudinary://895118841928427:Dgq7DEBxalY0cqUplPXTQx7b-rc@dkthctbvw',
      name: 'dkthctbvw',
      apiKey: '895118841928427',
      apiSecret: 'Dgq7DEBxalY0cqUplPXTQx7b-rc'
    }
  }
};