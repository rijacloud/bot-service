const httpStatus = require('http-status');

module.exports = class UserDetails {
  constructor(modules) {
    this._usersManager = modules.usersManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const fbid = req.params.fbid;
      const user = await this._usersManager.getUserByFbId(req.appId, fbid);
      return res.status(httpStatus.OK).json({ user });
    } catch (e) {
      return next(e);
    }
  }
};