const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const textDataSchema = new Schema({
  text: { type: String, required: true }
});

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: {
    type: textDataSchema,
    required: true
  },
  name: { type: String, required: true }
}, {
  timestamps: true
});

const projection = { 'data._id': 0 };

schema.statics.getByName = function({ app, name}) {
  return this.findOne({ app, name }, projection).lean();
};

schema.statics.getAndClean = function(id) {
  return this.findById(id, projection).lean();
};

const TextBlock = mongoose.model('textBlock', schema);

module.exports = TextBlock;