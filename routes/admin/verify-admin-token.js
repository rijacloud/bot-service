const httpStatus = require('http-status');
const _ = require('lodash');

module.exports = class VerifyAdminToken {
  constructor(modules) {
    this._adminManager = modules.adminManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const admin = await this._adminManager.findById(req.adminId);
      return res.status(httpStatus.OK).json({ admin });
      return res
    } catch (e) {
      return next(e);
    }
  }
}
