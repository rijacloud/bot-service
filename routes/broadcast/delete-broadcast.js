const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE } = require('../../constants/blocks');

module.exports = class DeleteBroadcast {
  constructor(modules) {
    this._broadcastManager = modules.broadCastManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);

  }

  async handler(req, res, next) {
    try {
      const { appId, id } = await this._getParameter(req.query, req.params);
      return (await this._deleteBroadcast(
        { appId, id },
        {}, res
      ));
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(query, params) {
    let appId = params.appId
    let id = params.id
    if (!id) this._badRequest('id paramameter is required')
    await this._appManager.checkAppId(appId);
    return { appId, id };
  }

  async _deleteBroadcast({ id }, data, res) {
    await this._broadcastManager.delete(id);
    console.log('Broadcast Deleted')  
    return res.status(httpStatus.OK).json({ ok: true });
  } 

  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }


}