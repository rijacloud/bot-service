const Joi = require('joi');

const carousselBlockSchema = Joi.compile(
  Joi.array().items(Joi.object({
    _id: Joi.string().optional(),
    title: Joi.string().required(),
    image_url: Joi.string().required(),
    subtitle: Joi.string().required(),
    default_action: Joi.object({
      type: Joi.string().required(),
      url: Joi.string().required(),
      webview_height_ratio: Joi.string().required()
    }).required(),
    buttons: Joi.array().items(Joi.object({
      _id: Joi.string().optional(),
      type: Joi.string().required(),
      title: Joi.string().required(),
      payload: Joi.when('type',
        { is: 'postback', then: Joi.string().required(), otherwise: Joi.forbidden() }
      ),
      url: Joi.when('type',
        { is: 'web_url', then: Joi.string().required(), otherwise: Joi.forbidden() }
      )
    }).required()).required()
  })).required()
);
const quickReplyBlockSchema = Joi.compile({
  _id: Joi.string().optional(),
  text: Joi.string().required(),
  quick_replies: Joi.array().items(Joi.object({
    _id: Joi.string().optional(),
    content_type: Joi.string().required(),
    title: Joi.string().required(),
    payload: Joi.string().required()
  })).required()
});
const buttonBLockSchema = Joi.compile({
  _id: Joi.string().optional(),
  attachment: Joi.object({
    type: Joi.string().required(),
    payload: Joi.object({
      template_type: Joi.string().required(),
      text: Joi.string().required(),
      buttons: Joi.array().items(Joi.object({
        _id: Joi.string().optional(),
        type: Joi.string().required(),
        title: Joi.string().required(),
        payload: Joi.string().required()
      }).required()).required()
    }).required()
  }).required()
});
const menuBLockSchema = Joi.compile(
  Joi.array().items(Joi.object({
    _id: Joi.string().optional(),
    type: Joi.string().required(),
    title: Joi.string().required(),
    payload: Joi.string().required()
  })).required()
);
const textBlockSchema = Joi.compile({
  _id: Joi.string().optional(),
  text: Joi.string().required()
});

const containerBLockSchema = Joi.compile({
  name: Joi.string().required(),
  data: Joi.array().items(Joi.object({
    _id: Joi.string().optional(),
    type: Joi.number().required(),
    name: Joi.string().required(),
    entry: Joi.any().required()
  })).required()
});

const mediaBLockSchema = Joi.compile(
  Joi.array().items(Joi.object({
    _id: Joi.string().optional(),
    mimeType: Joi.string().required(),
    attachment_id: Joi.string().required(),
    src: Joi.string().required()
  }).required()).required()
);

module.exports = {
  carousselBlockSchema,
  quickReplyBlockSchema,
  buttonBLockSchema,
  textBlockSchema,
  menuBLockSchema,
  containerBLockSchema,
  mediaBLockSchema
};
