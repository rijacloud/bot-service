const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const button = new Schema({
  content_type: { type: String, required: true },
  title: { type: String, required: true },
  payload: { type: String, required: true }
});

const buttonTemplate = new Schema({
  attachment: {
    type: { type: String, required: true },
    payload: {
      template_type: { type: String, required: true },
      text: { type: String, required: true },
      buttons: { type: [button], required: true }
    }
  }
});

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: {
    type: buttonTemplate,
    required: true
  },
  name: { type: String, required: true }
}, {
  timestamps: true
});

const projection = { 'data._id': 0, 'data.attachment.payload.buttons._id': 0 };
schema.statics.getByName = function({ app, name}) {
  return this.findOne({ app, name }, projection).lean();
};

schema.statics.getAndClean = function(id) {
  return this.findById(id, projection).lean();
}



const ButtonBLock = mongoose.model('buttonBLock', schema);

module.exports = ButtonBLock;