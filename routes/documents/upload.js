const httpStatus = require('http-status');
const fs = require('fs');
const moment = require('moment');

module.exports = class UploadFile {
  constructor(modules) {
    this._fileManager = modules.fileManager;
    this._appManager = modules.appManager;
    this._botClient = modules.botClient;

    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const fileObject = this._check(req.files);
      const { localPath, path, type } = this._buildPaths(fileObject, req.appId);
      const uploaded = await this._fileManager.upload(localPath, path, type);
      const image = await this._uploadAttachement(uploaded, req, type);
      return res.status(httpStatus.OK).json({ image });
    } catch (e) {
      return next(e);
    }
  }

  _check(files) {
    if (!files || !Object.keys(files).length) {
      const error = Error('No files were uploaded');
      error.statusCode = httpStatus.BAD_REQUEST;
      throw error;
    }
    return Object.values(files)[0];
  }

  _buildPaths(fileObject, appId) {
    const localPath = fileObject.tempFilePath;
    const fileName = fileObject.name;
    const type = fileObject.mimetype.split('/')[0];
    const year = moment().year();
    const month = moment().month();
    const path = `${appId}/${year}/${month}/${fileName}`;
    return { localPath, path, type };
  }

  async _uploadAttachement(uploaded, req, type) {
    if (!req.query.isAttachement) return uploaded;
    const appInstance = await this._appManager.getAppById(req.appId);
    const { attachment_id } = await this._botClient.uploadAttachement(
      appInstance.botHost, { src: uploaded.secure_url, type }
    );
    uploaded.attachment_id = attachment_id;
    return uploaded;
  }
}