const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE, CHANNEL } = require('../../constants/app');

module.exports = class CreateApp {
  constructor(modules) {
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);

    this._schema = Joi.compile({
      name: Joi.string().required(),
      type: Joi.number().allow(Object.values(TYPE)).required(),
      channel: Joi.number().when(
        'type',
        { is: TYPE.BOT, then: Joi.required(), otherwise: Joi.forbidden() }
      ),
      botHost: Joi.string().required()
    });

  }

  async handler(req, res, next) {
    try {
      const data = this._validate(req.body);
      await this._appManager.checkName(data.name);
      const entry = await this._appManager.create(
        data.name,
        data.type,
        {
          channel: data.channel,
          botHost: data.botHost
        }
      );
      return res.status(httpStatus.CREATED).json({ app: entry._doc });
    } catch (e) {
      return next(e);
    }
  }

  async _check(name) {
    const count = await this._appModel.count({ name });
    if (count) {
      const error = new Error(`App name ${name} already taken`);
      error.statusCode = httpStatus.BAD_REQUEST;
      throw error;
    }
    return true;
  }

  _validate(data) {
    const result = Joi.validate(data, this._schema);
    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }
}