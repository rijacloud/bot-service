const env = process.env.FMKL_ENV || 'DEV'; // env are DEV PROD or STAGING

module.exports = require(`./${env}`);
