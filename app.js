// App Config
const { APP } = require('./config');

const {
  port,
  mongoConnectionString,
  interserviceKey,
  interserviceAuthExpiration,
  saltRounds,
  cloudinaryConfig
} = APP;

// Internal dependencies
const SessionManager = require('./modules/session-manager');
const EnableCors = require('./middlewares/enable-cors');
const TokenAuth = require('./middlewares/token-auth');
const ErrorHandler = require('./middlewares/error-handler');
const InterserviceAuth = require('./middlewares/interservice');
const AppManager = require('./modules/app/app-manager');
const AdminManager = require('./modules/admin/admin-manager');
const UsersManager = require('./modules/users/users-manager');
const BlocksManager = require('./modules/blocks/blocks-manager');
const BroadCastManager = require('./modules/broadcast/broadcast-manager');
const cloudinary = require('cloudinary').v2;
cloudinary.config({ 
  cloud_name: cloudinaryConfig.name, 
  api_key: cloudinaryConfig.apiKey, 
  api_secret: cloudinaryConfig.apiSecret 
});
const FileManager = require('./modules/files');
const BotClient = require('./modules/bot-client');

//models
const appModel = require('./models/app-model');
const adminModel = require('./models/admin-model');
const userModel = require('./models/user-model');
const broadcastModel = require('./models/broadcast-model')


//blocks models
const containerBlockModel = require('./models/blocks/container-block-model');
const textBlockModel = require('./models/blocks/text-block-model');
const carousselBlockModel = require('./models/blocks/caroussel-block-model');
const quickReplyBlockModel = require('./models/blocks/quick-reply-block-model');
const buttonBLockModel = require('./models/blocks/button-block-model');
const menuBlockModel = require('./models/blocks/menu-block-model');
const mediaBlockModel = require('./models/blocks/media-block-model');

//utils
const sessionManager = new SessionManager({
  session: APP.session,
  fmklCreatorSession: APP.fmklCreatorSession
});
const enableCors = new EnableCors();
const tokenAuth = new TokenAuth({ sessionManager });
const errorHandler = new ErrorHandler();
const interserviceAuth = new InterserviceAuth({ interserviceKey })
const appManager = new AppManager({ appModel });
const adminManager = new AdminManager({ adminModel }, { saltRounds });
const usersManager = new UsersManager({ userModel });
const blocksManager = new BlocksManager(
  {
    textBlockModel,
    carousselBlockModel,
    quickReplyBlockModel,
    buttonBLockModel,
    containerBlockModel,
    menuBlockModel,
    mediaBlockModel
  }
);
const fileManager = new FileManager({ cloudinary });
const botClient = new BotClient(
  { appManager }, { interserviceKey, interserviceAuthExpiration }
);

const broadCastManager = new BroadCastManager({ broadcastModel, buttonBLockModel, textBlockModel,quickReplyBlockModel, carousselBlockModel })

// Internal dependencies
const ExpressBootstrapper = require('./modules/express-bootstrapper');
const mongoose = require('mongoose');
mongoose.Promise = Promise;

//routes
const Routes = require('./routes');
const TestRoute = require('./routes/test');
const AppRoute = require('./routes/app');
const AdminRoute = require('./routes/admin');
const UsersRoute = require('./routes/users');
const BLocksRoute = require('./routes/blocks');
const DocumentsRoute = require('./routes/documents');
const BroadcastRoute = require('./routes/broadcast')

// routes instances
const testRoute = new TestRoute({ tokenAuth }, { }, { }, { config: APP });
const appRoute = new AppRoute({ tokenAuth }, { }, { appManager })
const adminRoute = new AdminRoute(
  { tokenAuth },
  { },
  { appManager, adminManager, sessionManager },
  {  saltRounds }
);
const usersRoute = new UsersRoute(
  { interserviceAuth, tokenAuth },
  { },
  { usersManager },
  { }
);
const blocksRoute = new BLocksRoute(
  { tokenAuth, interserviceAuth },
  { },
  { blocksManager, appManager, botClient },
  { }
);
const broadcastRoute = new BroadcastRoute({ tokenAuth }, {}, { broadCastManager , appManager, botClient})
const documentsRoute = new DocumentsRoute(
  { tokenAuth },
  { },
  { fileManager, appManager, botClient }
);

// Mongo-connection
mongoose.connect(mongoConnectionString, { useNewUrlParser: true });

// Bootstrap
const expressBootstrapper = new ExpressBootstrapper(
  { errorHandler, enableCors }
);
expressBootstrapper.bootstrap();

const routes = new Routes(
  [
    testRoute,
    appRoute,
    adminRoute,
    usersRoute,
    blocksRoute,
    documentsRoute,
    broadcastRoute
  ],
  errorHandler
);

routes.initRoutes(expressBootstrapper.app);

expressBootstrapper.app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
