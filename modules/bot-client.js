const request = require('request-promise');
const jwt = require('jsonwebtoken');


module.exports = class BotClient {
  constructor(modules, config) {
    this._appManager = modules.appManager;
    this._interserviceKey = config.interserviceKey;
    this._interserviceAuthExpiration = config.interserviceAuthExpiration
  }

  auth(appId) {
    return jwt.sign({ appId }, this._interserviceKey, {
      expiresIn: this._interserviceAuthExpiration
    });
  }

  _post(url, data) {
    return request({
      url,
      headers: {
        authorization : this.auth()
      },
      timeout: 120000,
      method: 'POST',
      json: data
    })
  }

  updateMenu(botHost, body) {
    return this._post(`${botHost}/inter-service/menu`, body);
  }

  uploadAttachement(botHost, body) {
    return this._post(`${botHost}/inter-service/attachement`, body);
  }

  sendBroadcast(block, botHost) {
    return this._post(`${botHost}/inter-service/broadcast`, {block})
  }
}