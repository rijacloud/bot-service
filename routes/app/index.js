const express = require('express');
const CreateApp = require('./create-app');
const AppList = require('./app-list');

module.exports = class TestRoute {
  constructor(middlewares, models, modules, config) {
    this._tokenAuth = middlewares.tokenAuth;
    this._createApp = new CreateApp(modules);
    this._appList = new AppList(modules);
  }

  initRoutes(app) {
    const api = express.Router();
    api.post('/create', this._tokenAuth.fmklHandler, this._createApp.handler);
    api.get('/list', this._tokenAuth.fmklHandler, this._appList.handler);
    app.use('/app', api);
    console.log('initing app route');
  }
};
