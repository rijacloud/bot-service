const httpStatus = require('http-status');
const { TYPE } = require('../../constants/blocks');
const _ = require('lodash');

module.exports = class GetBlock {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this._appManager = modules.appManager;

    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const { type, name, appId } = await this._getParameter(req.query, req.params, req.appId);
      const block = await this._blocksManager.getByName(appId, name, type);
      return res.status(httpStatus.OK).json({ block });
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(query, params, appId) {
    let type = query.type;
    let name = query.name;
    if (!type) this._badRequest('type query parameter is required');
    if (!name) this._badRequest('name query parameter is required');
    type = parseInt(type, 10);
    if (!Object.values(TYPE).includes(type)) this._badRequest('invalid type');
    await this._blocksManager.isBLockExists(appId, name, type);
    return { type, name, appId };
  }

  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }
}