module.exports = class FileManager {
  constructor(modules) {
    this._cloudinary = modules.cloudinary;
    this._path = process.cwd();
  }

  upload(localPath, path, type) {
    try {
      return this._cloudinary.uploader.upload(
        localPath,
        { public_id: path, resource_type: type }
      );
    } catch (e) {
      throw e;
    }
  }
}