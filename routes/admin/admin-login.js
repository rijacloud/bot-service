const httpStatus = require('http-status');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const _ = require('lodash');

module.exports = class AdminLogin {
  constructor(modules) {
    this._sessionManager = modules.sessionManager;
    this._adminManager = modules.adminManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);

    this._schema = Joi.compile({
      email: Joi.string().required(),
      password: Joi.string().required()
    });
  }

  async handler(req, res, next) {
    try {
      const data = this._validate(req.body);
      const appId = await this._appManager.checkAppId(req.params.appId);
      const { token, admin } = await this._check(data, appId);
      return res.status(httpStatus.OK).json({ token, admin });
    } catch (e) {
      return next(e);
    }
  }

  async _check(data, appId) {
    const admin = await this._adminManager.findByEmail(data.email, appId);
    if (!admin || !(await bcrypt.compare(data.password, admin.hash))) {
      const err = new Error('Mauvais login');
      err.statusCode = httpStatus.UNAUTHORIZED;
      throw err;
    }
    const token = this._generateToken(admin, appId);
    return { token, admin: _.omit(admin, ['hash']) };
  }

  _generateToken(admin, appId) {
    return this._sessionManager.signIn({
      adminId: admin._id,
      appId
    });
  }

  _validate(data) {
    const result = Joi.validate(data, this._schema);
    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }
}