const httpStatus = require('http-status');
const Joi = require('joi');

module.exports = class AddUser {
  constructor(modules) {
    this._usersManager = modules.usersManager;
    this.handler = this.handler.bind(this);
    this._schema = Joi.compile({
      fbid: Joi.string().required(),
      first_name: Joi.string().required(),
      last_name: Joi.string().required(),
      profile_pic: Joi.string().required()
    });
  }

  async handler(req, res, next) {
    try {
      const appId = req.appId;
      const data = this._validate(req.body);
      const user = await this._usersManager.create({
        ...data,
        appId
      });
      return res.status(httpStatus.OK).json({ user });
    } catch (e) {
      return next(e);
    }
  }

  _validate(data) {
    const result = Joi.validate(data, this._schema);
    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }
};
