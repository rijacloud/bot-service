const httpStatus = require('http-status');
const bcrypt = require('bcrypt');

module.exports = class AdminManager {
  constructor(models, config) {
    this._adminModel = models.adminModel;
    this._saltRounds = config.saltRounds;
  }

  async checkEmail(appId, email) {
    const count = await this._adminModel.countDocuments(
      { app: appId, email }
    );
    if (!count) return true;
    const err = new Error(`Admin with email ${email} exists under the app ${appId}`);
    err.statusCode = httpStatus.BAD_REQUEST;
    throw err;
  }

  async create({ app, email, access, password }) {
    const hash = await bcrypt.hash(password, this._saltRounds);
    return this._adminModel.create({
      app,
      email,
      access,
      hash
    });
  }

  findByEmail(email, appId) {
    return this._adminModel.findOne({ email, app: appId }).lean();
  }

  findById(_id) {
    return this._adminModel.findById(_id, { hash: 0 }).lean();
  }
};
