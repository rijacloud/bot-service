const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');

module.exports = class InterserviceAuth {
  constructor(config) {
    this._interserviceKey = config.interserviceKey;
    this.check = this.check.bind(this);
  }

  async check(req, res, next) {
    try {
      const { appId } = await this._checkToken(req.headers.authorization || '', this._interserviceKey);
      req.appId = appId;
      return next();
    } catch (e) {
      const error = new Error('wrong interservice token');
      error.statusCode = httpStatus.UNAUTHORIZED;
      return next(error);
    }
  }

  _checkToken(token, privateKey) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, privateKey, (err, decoded) => {
        if (err) return reject(err.name);
        return resolve(decoded);
      });
    });
  }
}