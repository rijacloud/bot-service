const express = require('express');
const AddUser = require('./add-user');
const CountUsers = require('./count-users');
const UserDetails = require('./user-details');
const listUsers = require('./list-user')

module.exports = class TestRoute {
  constructor(middlewares, models, modules, config) {
    this._interserviceAuth = middlewares.interserviceAuth;
    this._tokenAuth = middlewares.tokenAuth;
    this._addUser = new AddUser(modules);
    this._countUsers = new CountUsers(modules);
    this._userDetails = new UserDetails(modules);
    this._listUsers = new listUsers(modules)
  }

  initRoutes(app) {
    const api = express.Router();
    api.post('/add', this._interserviceAuth.check, this._addUser.handler);
    api.get('/count', this._interserviceAuth.check, this._countUsers.handler);
    api.get('/details/:fbid', this._interserviceAuth.check, this._userDetails.handler);
    api.get('/list' , this._interserviceAuth.check, this._listUsers.handler);
    app.use('/users', api);
  }
};
