module.exports = {
  discoverCars: [
    {
      "title": "Berline",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1581053600/Karengy_ni60t1.jpg",
      "subtitle": "Berline, 6 places, idéal pour les familles ou personnes cherchant un véhicule polyvalent avec sa banquette arrière rabattable",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "Berline",
              "payload": "discover_cars_choose_name=Berline&_5e3ab9cd4d1ad74578a4c65f"
            }              
        ]
    },
    {
      "title": "Pick up",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1581053600/DiY0z-oW0AA01LZ_fpygqe.jpg",
      "subtitle": "Pick-up, 6 places, avec possibilité de rajouter une galerie sur le toit et un couvre benne.",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "Pick up",
              "payload": "discover_cars_choose_name=Pick up&_5e3ab9ce4d1ad74578a4c662"
            }              
        ]
    },
    {
      "title": "Fourgon",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1581053599/ECVsrRWXkAA2cqP_rzqjen.jpg",
      "subtitle": "Fourgon, 3 places, pour les entreprises voulant concilier économie à l'usage, robustesse et volume de chargement important",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "Fourgon",
              "payload": "discover_cars_choose_name=Fourgon&_5e3ab9cf4d1ad74578a4c665"
            }              
        ]
    }
  ],
  chooseMotor: [
    {
      "title": "4X4 Extreme",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1580990860/4x4_extreme_gdqmaq.png",
      "subtitle": "4x4 extrême, avec son blocage de différentiel arrière permettant de se sortir des mauvaises situations",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "4X4 Extreme",
              "payload": "customize_cars_edit_name=${param.name}&motor=4x4bloc&step=2_${param.id}"
            }              
        ]
    },
    {
      "title": "4X4 Endurance",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1580990860/4x4_extreme_gdqmaq.png",
      "subtitle": "4x4 endurance, pour ceux sortant parfois des sentiers battus le temps d'un WE, avec un système \"automatique\" pour plus de simplicité",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "4X4 Endurance",
              "payload": "customize_cars_edit_name=${param.name}&motor=4x4&step=2_${param.id}"
            }              
        ]
    },
    {
      "title": "4X2",
      "image_url": "https://res.cloudinary.com/dkthctbvw/image/upload/v1580990860/4x2_miqh9z.png",
      "subtitle": "4x2, idéal pour un usage principalement citadin et route goudronnée",
      "default_action": {
            "type": "web_url",
            "url": "http://www.karenjy.mg/rubriques/la-mazanaii",
            "webview_height_ratio": "FULL"
        },
        "buttons":[
          {
              "type": "postback",
              "title": "4X2",
              "payload": "customize_cars_edit_name=${param.name}&motor=4x2&step=2_${param.id}"
            }              
        ]
    }
  ]
};