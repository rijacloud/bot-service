const httpStatus = require('http-status');

module.exports = class GetBlocksByAppId {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const appId = await this._check(req);
      const blocks = await this._blocksManager.getByAppId(appId);
      return res.status(httpStatus.OK).json({ blocks });
    } catch (e) {
      return next(e);
    }
  }

  async _check(req) {
    await this._appManager.checkAppId(req.appId);
    return req.appId;
  }
}