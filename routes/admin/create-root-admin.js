const httpStatus = require('http-status');
const Joi = require('joi');
const { ACCESS } = require('../../constants/admin');
const _ = require('lodash');

module.exports = class CreateRootAdmin {
  constructor(modules) {
    this._adminManager = modules.adminManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);
    this._schema = Joi.compile({
      email: Joi.string().required(),
      password: Joi.string().required()
    });
  }

  async handler(req, res, next) {
    try {
      const data = this._validate(req.body);
      const appId = await this._appManager.checkAppId(req.params.appId);
      await this._adminManager.checkEmail(appId, data.email);
      const admin = await this._create(data, appId);
      return res.status(httpStatus.CREATED).json({ admin: _.omit(admin, ['hash']) });
    } catch (e) {
      return next(e);
    }
  }

  async _create(data, app) {
    const access = [ACCESS.ROOT];
    const entry = await this._adminManager.create({ ...data, app, access });
    return entry._doc;
  }

  _validate(data) {
    const result = Joi.validate(data, this._schema);
    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
  }
};
