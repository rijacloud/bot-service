const express = require('express');
const CreateBlock = require('./create-block');
const GetBLock = require('./get-block');
const GetBLocksByAppId = require('./get-blocks-by-app-id');
const UpdateBLocks = require('./update-blocks');
const GetContainer = require('./get-container');
const DeleteContainer = require('./delete-container');

module.exports = class TestRoute {
  constructor(middlewares, models, modules, config) {
    this._interserviceAuth = middlewares.interserviceAuth;
    this._tokenAuth = middlewares.tokenAuth;
    this._createBlock = new CreateBlock(modules);
    this._getBlock = new GetBLock(modules);
    this._getBLocksByAppId = new GetBLocksByAppId(modules);
    this._updateBLocks = new UpdateBLocks(modules);
    this._getContainer = new GetContainer(modules);
    this._deleteContainer = new DeleteContainer(modules);
  }

  initRoutes(app) {
    const api = express.Router();
    
    api.post('/:appId/create', this._tokenAuth.fmklHandler, this._createBlock.handler);
    api.post('/generate', this._tokenAuth.handler, this._createBlock.handler);
    api.get('/', this._interserviceAuth.check, this._getBlock.handler);
    api.get('/app-scope', this._tokenAuth.handler, this._getBLocksByAppId.handler);
    api.get('/container/:containerId', this._interserviceAuth.check, this._getContainer.handler);
    api.patch('/:blockId/update', this._tokenAuth.handler, this._updateBLocks.handler);
    api.delete('/:blockId', this._tokenAuth.handler, this._deleteContainer.handler);
    app.use('/blocks', api);
  }
};
