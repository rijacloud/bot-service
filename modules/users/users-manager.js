const httpStatus = require('http-status');

module.exports = class UsersManager {
  constructor(models) {
    this._userModel = models.userModel;
  }

  async create({ appId, first_name, last_name, profile_pic, fbid }) {
    const exist = await this.findByFbid(fbid);
    if (exist) return exist;
    const entry = await this._userModel.create({
      app: appId,
      first_name,
      last_name,
      profile_pic,
      channelId: fbid
    });
    return entry._doc;
  }

  findByFbid(fbid) {
    return this._userModel.findOne({ channelId: fbid }).lean();
  }

  findUsersByAppId(appId) {
    return this._userModel.find({ app: appId }).lean();
  }

  countUsersByAppId(appId) {
    return this._userModel.countDocuments({ app: appId });
  }

  async getUserByFbId(appId, fbid) {
    const user = await this._userModel.findOne({ app: appId, channelId: fbid }).lean();
    if (!user) {
      const error = Error('Utilisateur non trouve');
      error.statusCode = httpStatus.NOT_FOUND;
      throw error;
    }
    return user;
  }

  findUsersListByAppId(appId) {
    return this._userModel.find({ app: appId });
  }
};
