const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE } = require('../../constants/blocks');


module.exports = class CallBroadcast {
  constructor(modules) {
    this._broadcastManager = modules.broadCastManager;
    this._appManager = modules.appManager;
    this._botClient = modules.botClient
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const query = req.query
      return (await this._callBroadcast(
        { query }, res
      ));
    } catch (e) {
      return next(e);
    }
  }

  async _callBroadcast({ query }, res) {
    const broadast = await this._broadcastManager.getByDate(query)
    if(broadast) {
      for(let b in broadast) {
        console.log('In progress : ' + broadast[b]._id)  
        let appInstance = await this._appManager.getAppById(broadast[b].app)
        await this._botClient.sendBroadcast(broadast[b].data, appInstance.botHost)
        console.log('Broadcast sent : ' + broadast[b]._id)
      }
    }
    return res.status(httpStatus.OK).json({ ok: true });
  } 

}