const httpStatus = require('http-status');

module.exports = class ListUser {
  constructor(modules) {
    this._usersManager = modules.usersManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const users = await this._usersManager.findUsersListByAppId(req.appId);
      return res.status(httpStatus.OK).json({ users });
    } catch (e) {
      return next(e);
    }
  }
};