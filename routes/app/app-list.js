const httpStatus = require('http-status');

module.exports = class AppList {
  constructor(modules) {
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const apps = await this._appManager.list();
      return res.status(httpStatus.OK).json({ apps });
    } catch (e) {
      return next(e);
    }
  }
};
