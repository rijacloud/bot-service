const httpStatus = require('http-status');
const env = require('../../config/DEV')
const axios = require('axios')

module.exports = class BroadCastManager {
    constructor(models) {
        this.model = models.broadcastModel
        this.textModel = models.textBlockModel
    }

    create(appId, data, name, executionDate) {
        
        return this.model.create({
            app: appId,
            name,
            data,
            executionDate
        })
    }

    async update(appId,data, name, executionDate) {
        
        return await this.model.findOneAndUpdate({
            name, 
            app: appId
        }, {
            executionDate,
            data
        })   
    }

    async checkName(appId, name) {
        const count = await this.model.countDocuments({ app: appId, name });
        return (count) ? true : false;
    }

    getList(appId) {
        return this.model.find({
            app: appId
        }, {
            '__v': 0,
        })
        .sort({ createdAt: 'desc' })
    }  

    getOne(id) {
        return this.model.findOne({
            _id: id
        }, {
            '__v': 0
        })
    }

    delete(id) {
        return this.model.deleteOne({
            "_id": id
        })
    }

    getByDate(date) {
        return this.model.find({
            "executionDate.day": parseInt(date.day),
            "executionDate.month": parseInt(date.month),
            "executionDate.hour" : parseInt(date.hour),
            "executionDate.year": parseInt(date.year),
            "executionDate.minute": parseInt(date.min),
        })
    }

}
