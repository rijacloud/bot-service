const { APP } = require('../config');

const SessionManager = require('../modules/session-manager');

const sessionManager = new SessionManager({
  session: APP.session,
  fmklCreatorSession: APP.fmklCreatorSession
});

const payload = {
  random: true
};

const token = sessionManager.signFmkl(payload);

console.log('here is your fmkl token : ', token);