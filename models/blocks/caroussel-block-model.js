const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const button = new Schema({
  type: { type: String, required: true },
  title: { type: String, required: true },
  payload: {
    type: String,
    required: function () {
      return this.type === 'postback';
    }
  },
  url: {
    type: String,
    required: function () {
      return this.type === 'web_url';
    }
  }
});

const element = new Schema({
  title: { type: String, required: true },
  image_url: { type: String, required: true },
  subtitle: { type: String, required: true },
  default_action: {
    type: { type: String, required: true },
    url: { type: String, required: true },
    webview_height_ratio: { type: String, required: true }
  },
  buttons: [button]
});

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: {
    type: [element],
    required: true
  },
  name: { type: String, required: true }
}, {
  timestamps: true
});

const projection = { 'data._id': 0, 'data.buttons._id': 0 };

schema.statics.getByName = function({ app, name }) {
  return this.findOne(
    { app, name },
    projection
  ).lean();
};

schema.statics.getAndClean = function(id) {
  return this.findById(id, projection).lean();
};

const CarousselBLock = mongoose.model('carousselBLock', schema);

module.exports = CarousselBLock;