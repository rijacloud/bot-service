const httpStatus = require('http-status');
const { TYPE } = require('../../constants/blocks');

module.exports = class getContainer {
  constructor(modules) {
    this._blocksManager = modules.blocksManager;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const { containerId } = await this._getParameter(req.params);
      let block = await this._blocksManager.getById(containerId, TYPE.CONTAINER);
      block = await this._blocksManager.populateAndClean(block);
      return res.status(httpStatus.OK).json({ block });
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(params) {
    const containerId = params.containerId;
    if (!containerId) this._badRequest('containerid parameter is required');
    await this._blocksManager.checkBlockId(containerId, TYPE.CONTAINER);
    return { containerId };
  }

  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }
}