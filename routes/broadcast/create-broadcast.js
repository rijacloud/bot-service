const httpStatus = require('http-status');
const Joi = require('joi');
const { TYPE } = require('../../constants/blocks');
const {
    broadcastSchema
} = require('./schemas')

module.exports = class CreateBroadcast {
  constructor(modules) {
    this._broadcastManager = modules.broadCastManager;
    this._appManager = modules.appManager;
    this._botClient = modules.botClient
    this.handler = this.handler.bind(this);

  }

  async handler(req, res, next) {
    try {
      const { name, appId, exist, only } = await this._getParameter(req.query, req.params);
      return (await this._addBroadcast(
        { appId, name, only },
        req.body, res, exist
      ));
    } catch (e) {
      return next(e);
    }
  }

  async _getParameter(query, params) {
    let name = query.name;
    let appId = params.appId
    let only = query.only
    if (!name) this._badRequest('name query parameter is required');
    const exist = await this._broadcastManager.checkName(appId, name);
    await this._appManager.checkAppId(appId);
    return { name, appId, exist, only };
  }

  async _addBroadcast({ appId, name,only }, data, res, exist) {
    data = this._validate(data, broadcastSchema);
    let entry = []
    if(!exist) {
      entry = await this._broadcastManager.create(
        appId,
        data.data,
        name,
        data.executionDate
      );
      console.log('Broadcast created')
    } else {
      entry = await this._broadcastManager.update(
        appId,
        data.data,
        name,
        data.executionDate
      );
      console.log('Broadcast Updated')
    }

    const appInstance = await this._appManager.getAppById(appId)
    if(!only) {
      await this._botClient.sendBroadcast(data.data, appInstance.botHost)
    }
    return res.status(httpStatus.OK).json({ broadcast: entry });
    
  }
  _badRequest(message) {
    const error = Error(message);
    error.statusCode = httpStatus.BAD_REQUEST;
    throw error;
  }

  _validate(data, schema) {
     
    const result = Joi.validate(data, schema);

    if (result.error) {
      const err = new Error(result.error.details[0].message);
      err.statusCode = httpStatus.BAD_REQUEST;
      throw err;
    }
    return result.value;
    
  }

}