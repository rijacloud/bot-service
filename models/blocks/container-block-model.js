const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: [{
    type: { type: String, required: true },
    id: { type: ObjectId, required: true }
  }],
  name: { type: String, required: true }
}, {
  timestamps: true
});

schema.statics.getByName = function({ app, name}) {
  return this.findOne({ app, name }).lean();
};

const ContainerBlock = mongoose.model('containerBlock', schema);

module.exports = ContainerBlock;