const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  app: { type: ObjectId, required: true, ref: 'app' },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  profile_pic: { type: String, required: true },
  channelId: { type: String, required: true, uniq: true },
  optional: {
    name: String,
    phone: String,
    email: String
  }
}, {
  timestamps: true
});
const User = mongoose.model('user', schema);

module.exports = User;
