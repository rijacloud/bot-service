const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { TYPE, CHANNEL } = require('../constants/app');

const schema = new Schema({
  name: { type: String, uniq: true, required: true },
  type: { type: Number, required: true },
  channel: {
    type: Number,
    required: function() {
      return this.type === TYPE.BOT;
    }
  },
  botHost: { type: String }
}, {
  timestamps: true
});
const App = mongoose.model('app', schema);

module.exports = App;
