const httpStatus = require('http-status');

module.exports = class GetBroadcast {
  constructor(modules) {
    this._broadcastManager = modules.broadCastManager;
    this._appManager = modules.appManager;
    this.handler = this.handler.bind(this);

  }

  async handler(req, res, next) {
    //do nothing
    return res.status(httpStatus.OK).json({ ok : true });
  }

}