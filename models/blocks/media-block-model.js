const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const element = new Schema({
  mimeType: { type: String, required: true },
  attachment_id: { type: String, required: true },
  src: { type: String, required: true }
});

const schema = new Schema({
  app: { type: ObjectId, required: true },
  type: { type: Number, required: true },
  data: {
    type: [element],
    required: true
  },
  name: { type: String, required: true }
}, {
  timestamps: true
});

const projection = { 'data._id': 0 };

schema.statics.getByName = function({ app, name }) {
  return this.findOne(
    { app, name },
    projection
  ).lean();
};

schema.statics.getAndClean = async function(id) {
  const result = await this.findById(id, projection).lean();
  result.data = result.data.map(element => {
    return {
      attachment_id: element.attachment_id,
      media_type: element.mimeType.split('/')[0]
    };
  });
  return result;
};

const MediaBLock = mongoose.model('mediaBLock', schema);

module.exports = MediaBLock;
